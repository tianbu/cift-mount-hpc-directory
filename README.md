# cift mount HPC directory
example under ubuntu

1. install cifs
sudo apt install cifs-utils

2. edit fstab
sudo vim fstab
added "//hpc-fs.university.edu.au/abcd   /mnt/univeristy_hpc_home   cifs    user,rw,domain=QUTAD,user=abcd,credentials=/home/abcd/.university_cifs_credentials,uid=abcd 0   0"

3. mkdir directory
sudo mkdir /mnt/univeristy_hpc_home

4. test
sudo mount -a 

